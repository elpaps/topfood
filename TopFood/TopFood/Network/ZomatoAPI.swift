//
//  ZomatoAPI.swift
//  TopFood
//
//  Created by elpaps on 10/12/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import Foundation
import Combine


class RestaurantFetcher {
  private let session: URLSession
  init(session: URLSession = .shared) {
    self.session = session
  }
}

protocol RestaurantFetchable {
  func searchTopRestaurants(
    forQuery query: String,start item:Int
  ) -> AnyPublisher<RestaurantList, ZomatoError>

}


// MARK: - MoviesFetchable
extension RestaurantFetcher: RestaurantFetchable {
    
 func searchTopRestaurants(
    forQuery query: String,start item:Int
 ) -> AnyPublisher<RestaurantList, ZomatoError> {
    return request(with: searhRestaurantComponents(withQuery: query, start: item))
  }

  private func request<T>(
    with components: URLComponents
  ) -> AnyPublisher<T, ZomatoError> where T: Decodable {
    guard let url = components.url else {
      let error = ZomatoError.network(description: "Couldn't create URL")
      return Fail(error: error).eraseToAnyPublisher()
    }
    var request = URLRequest(url: url)
    request.addValue(ZomatoAPI.key, forHTTPHeaderField: "user-key")
  
    return session.dataTaskPublisher(for: request)
      .mapError { error in
        .network(description: error.localizedDescription)
      }
      .flatMap(maxPublishers: .max(1)) { pair in
        decode(pair.data)
      }
      .eraseToAnyPublisher()
  }
}


// MARK: - trakt API
private extension RestaurantFetcher {
    struct ZomatoAPI {
        static let scheme = "https"
        static let host = "developers.zomato.com"
        static let path = "/api/v2.1"
        static let key = "2d56bcd20cb6475efcd4cc1d85796b2b"
    }

  func searhRestaurantComponents(
    withQuery query:String,start item:Int
   ) -> URLComponents {
     var components = URLComponents()
     components.scheme = ZomatoAPI.scheme
     components.host = ZomatoAPI.host
     components.path = ZomatoAPI.path + "/search"
     
     components.queryItems = [
       URLQueryItem(name: "q", value: query),
       URLQueryItem(name: "sort", value: "rating"),
       URLQueryItem(name: "start", value: "\(item)"),
       URLQueryItem(name: "count", value: "20")
     ]
     return components
   }
}

