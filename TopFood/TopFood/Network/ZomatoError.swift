//
//  ZomatoError.swift
//  TopFood
//
//  Created by elpaps on 10/12/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import Foundation

enum ZomatoError: Error {
  case parsing(description: String)
  case network(description: String)
}
