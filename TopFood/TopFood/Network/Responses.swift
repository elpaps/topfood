//
//  Response.swift
//  TopFood
//
//  Created by elpaps on 10/12/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import Foundation

struct RestaurantList:Codable {
    let restaurants:[[String:RestaurantResponse]]
}

struct RestaurantResponse: Codable {
    let id: String?
    let name: String?
    let location:Location?
    let thumb:String?
    let cuisines:String?
    let user_rating:Rating?
    let menu_url:String?
    let phone_numbers:String?
}

struct Location:Codable {
    
    let address:String?
    let city:String?
    let latitude:String?
    let longitude:String?
}

struct Rating:Codable {
    let aggregate_rating:String?
    let rating_text:String?
    let votes:String?
}
