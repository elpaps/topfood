//
//  TopFoodRowViewModel.swift
//  TopFood
//
//  Created by elpaps on 10/12/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import Foundation

import SwiftUI

struct TopFoodRowViewModel:Identifiable {
    private let restaurant: [String:RestaurantResponse]
    
    init(restaurant: [String:RestaurantResponse]) {
        self.restaurant = restaurant
    }
    
    var id: String {
        guard let id  = restaurant["restaurant"]?.id else { return ""}
        return id
    }
    var name: String {
        guard let name = restaurant["restaurant"]?.name else {return ""}
        return name
    }
    
    var cuisines: String {
        guard let cuisines = restaurant["restaurant"]?.cuisines else {return ""}
        return cuisines
    }
    
    var rating: Float {
        guard let rating = restaurant["restaurant"]?.user_rating?.aggregate_rating else {return 0}
        return Float(rating) ?? 0
    }
    var votes: String {
        guard let votes = restaurant["restaurant"]?.user_rating?.votes else {return ""}
        return votes
    }
    var titleRating: String {
        guard let title = restaurant["restaurant"]?.user_rating?.rating_text else {return ""}
        return title
       }
    var thumb: URL? {
        guard let thumb = restaurant["restaurant"]?.thumb else {return nil}
        return URL(string: thumb)
    }
    var address:String {
        guard let address = restaurant["restaurant"]?.location?.address else { return ""}
        return address
    }
    var city:String {
        guard let city = restaurant["restaurant"]?.location?.city else { return ""}
        return city
    }
    var latitude:String {
        guard let latitude = restaurant["restaurant"]?.location?.latitude else { return ""}
        return latitude
    }
    var longitude:String {
        guard let longitude = restaurant["restaurant"]?.location?.longitude else { return ""}
        return longitude
    }
    
    var phones:String {
        guard let phones = restaurant["restaurant"]?.phone_numbers else { return ""}
        return phones
    }
    
    var menu:String {
        guard let menu = restaurant["restaurant"]?.menu_url else { return ""}
        return menu
    }
    
}
