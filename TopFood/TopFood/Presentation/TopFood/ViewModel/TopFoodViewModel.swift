//
//  TopFoodViewModel.swift
//  TopFood
//
//  Created by elpaps on 10/12/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

class TopFoodViewModel: ObservableObject {
  @Published var restaurant: String = "" {
    didSet {
        dataSource.removeAll()
        start = -20
    }
  }
  @Published var dataSource: [TopFoodRowViewModel] = []

  private let restaurantFetcher: RestaurantFetchable
  private var disposables = Set<AnyCancellable>()
  private var start = -20
    
    init(
        restaurant: RestaurantFetchable,
        scheduler: DispatchQueue = DispatchQueue(label: "MovieViewModel")
    ) {
        self.restaurantFetcher = restaurant
        _ = $restaurant
            .dropFirst(1)
            .debounce(for: .seconds(0.5), scheduler: scheduler)
            .sink(receiveValue: fetchTopFoods(forQuery:))
    }
    
    func getTopFood() {
        start += 20
        fetchTopFoods(forQuery: restaurant)
    }

    func fetchTopFoods(forQuery query: String="") {
        
        restaurantFetcher.searchTopRestaurants(forQuery: query, start: start).map { response in
            response.restaurants.map(TopFoodRowViewModel.init)
        }
        .receive(on: DispatchQueue.main)
        .sink(
            receiveCompletion: { [weak self] value in
                guard let self = self else { return }
                switch value {
                case .failure:
                    self.dataSource = []
                case .finished:
                    break
                }
            },
            receiveValue: { [weak self] restaurants in
                guard let self = self else { return }
                self.dataSource += restaurants
        })
            .store(in: &disposables)
    }

}
