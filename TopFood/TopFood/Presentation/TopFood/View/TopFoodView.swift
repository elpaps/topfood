//
//  TopFoodView.swift
//  TopFood
//
//  Created by elpaps on 10/12/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import SwiftUI

struct TopFoodView: View {
    @ObservedObject var viewModel: TopFoodViewModel
    var body: some View {
      NavigationView {
        List {
            searchField
            if viewModel.dataSource.isEmpty {
                emptySection
            }else{
                moviesSection
            }
            Rectangle().foregroundColor(.clear).onAppear {
                self.viewModel.getTopFood()
            }
        }
        .listStyle(GroupedListStyle())
        .navigationBarTitle("Restaurants")
      }
    }
    
}

private extension TopFoodView {
    
    var searchField: some View {
        HStack(alignment: .center) {
            TextField("Search restaurant", text: $viewModel.restaurant)
        }
    }
    var moviesSection: some View {
      Section {
//        ForEach(viewModel.dataSource, content: TopFoodRowView.init(viewModel:))

        ForEach(viewModel.dataSource) { model  in
            NavigationLink(destination: TopFoodViewDetal(viewModel: model)) {
                TopFoodRowView.init(viewModel: model).scaledToFit()
            }
        }
      }
    }
    var emptySection: some View {
        Section {
            Text("No results")
                .foregroundColor(.gray)
        }
    }
}
