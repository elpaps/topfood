//
//  TopFoodRowView.swift
//  TopFood
//
//  Created by elpaps on 10/12/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import SwiftUI
import Cosmos

struct TopFoodRowView: View {
  private let viewModel: TopFoodRowViewModel
  
   init(viewModel: TopFoodRowViewModel) {
        self.viewModel = viewModel
    }
  
    var body: some View {
        HStack {
            ImageViewWidget(imageUrl: viewModel.thumb)
            VStack(alignment: .leading) {
                Text("\(viewModel.name)")
                    .font(.body)
                Text("\(viewModel.cuisines)")
                    .font(.system(size: 11)).lineLimit(2)
                HStack {
                    RatingView(rating: viewModel.rating)
                    Text(viewModel.titleRating)
                        .font(.system(size: 11))
                    Text("(\(viewModel.votes)" + " votes)").font(.system(size: 11))
                    .font(.body)
                }.fixedSize().padding(.trailing,8)
                Text("\(viewModel.city)")
                    .font(.caption)
            }.padding(.leading, 8)
        }.padding(.trailing,8)
    }
}

struct ImageViewWidget:View {
    
    @ObservedObject var imageLoader: ImageLoader
    var imageUrl : URL?
       
    init(imageUrl: URL?,imageLoader:ImageLoader = ImageLoader()) {
        self.imageLoader = imageLoader
        self.imageUrl = imageUrl
    }
       
    var body: some View {
        Image(uiImage:imageLoader.image(for: imageUrl))
            .resizable()
            .frame(width: 100, height: 70)
            .clipShape(Rectangle())
            .overlay(Rectangle().stroke(Color.gray, lineWidth: 1))
    }
}


struct RatingView : UIViewRepresentable {
    var rating:Float
    typealias UIViewType = CosmosView
    
    func makeUIView(context: UIViewRepresentableContext<RatingView>) -> CosmosView {
        let view = CosmosView(settings: CosmosSettings())
        view.frame = CGRect(x: 0, y: 0, width: 20, height: 0)
        view.settings.fillMode = .precise
        view.settings.starSize = 18
        view.rating = Double(rating)
        return view
    }
    func updateUIView(_ uiView: CosmosView, context: UIViewRepresentableContext<RatingView>) {
        uiView.frame = CGRect(x: 0, y: 0, width: 20, height: 5)
    }
    
}

