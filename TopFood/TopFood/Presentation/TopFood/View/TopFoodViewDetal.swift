//
//  TopFoodViewDetal.swift
//  TopFood
//
//  Created by elpaps on 10/13/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import SwiftUI
import CoreLocation
import SafariServices

struct TopFoodViewDetal: View {
    var viewModel: TopFoodRowViewModel
    @State var shown = false
    var body: some View {
        VStack {
            MapView(coordinate: CLLocationCoordinate2D(
            latitude: Double(viewModel.latitude)!,
            longitude: Double(viewModel.longitude)!))
                .frame(height: 300)
            Image(uiImage:ImageLoader().image(for: viewModel.thumb)).frame(width: 150, height: 150, alignment: .center).clipShape(Circle())
                .overlay(Circle().stroke(Color.white, lineWidth: 4))
                .shadow(radius: 10)
                .offset(x: 0, y: -100)
                .padding(.bottom, -100)

            List {
                HStack {
                    Text("City:").bold().font(.body)
                    Text("\(viewModel.city)").font(.body)
                }
                HStack {
                    Text("Address:").bold().font(.body)
                    Text("\(viewModel.address)").font(.body)
                }
                HStack {
                    Text("Menu:").bold().font(.body)
                    Button(action: {
                        self.shown.toggle()
                    }) {
                         Text("\(viewModel.menu)").foregroundColor(.blue).font(.body)
                    }
                }.sheet(isPresented: $shown) { () -> SafariView in
                    return SafariView(url: URL(string: self.viewModel.menu)!)
                }
                HStack {
                    Text("Phones:").bold().font(.body)
                    Text("\(viewModel.phones)").font(.body)
                }
            }.accentColor(.clear)
             Spacer()
            }
        .navigationBarTitle(Text(verbatim: viewModel.name), displayMode: .inline)
    }
}


struct SafariView: UIViewControllerRepresentable {
    typealias UIViewControllerType = CustomSafariViewController

    var url: URL?

    func makeUIViewController(context: UIViewControllerRepresentableContext<SafariView>) -> CustomSafariViewController {
        return CustomSafariViewController()
    }

    func updateUIViewController(_ safariViewController: CustomSafariViewController, context: UIViewControllerRepresentableContext<SafariView>) {
        safariViewController.url = url
    }
}

final class CustomSafariViewController: UIViewController {
    var url: URL? {
        didSet {
            // when url changes, reset the safari child view controller
            configureChildViewController()
        }
    }

    private var safariViewController: SFSafariViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        configureChildViewController()
    }

    private func configureChildViewController() {
        // Remove the previous safari child view controller if not nil
        if let safariViewController = safariViewController {
            safariViewController.willMove(toParent: self)
            safariViewController.view.removeFromSuperview()
            safariViewController.removeFromParent()
            self.safariViewController = nil
        }

        guard let url = url else { return }

        // Create a new safari child view controller with the url
        let newSafariViewController = SFSafariViewController(url: url)
        addChild(newSafariViewController)
        newSafariViewController.view.frame = view.frame
        view.addSubview(newSafariViewController.view)
        newSafariViewController.didMove(toParent: self)
        self.safariViewController = newSafariViewController
    }
}
