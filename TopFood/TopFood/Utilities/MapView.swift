//
//  MapView.swift
//  TopFood
//
//  Created by elpaps on 10/13/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import SwiftUI
import MapKit

struct MapView: UIViewRepresentable {
    var coordinate: CLLocationCoordinate2D

    func makeUIView(context: Context) -> MKMapView {
        MKMapView(frame: .zero)
    }

    func updateUIView(_ view: MKMapView, context: Context) {
        let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        let originAnnotation = MKPointAnnotation()
        originAnnotation.title = "origin"
        originAnnotation.coordinate = coordinate
        view.addAnnotation(originAnnotation)
        view.setRegion(region, animated: true)
    }
}

