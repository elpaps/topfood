//
//  CircleImage.swift
//  TopFood
//
//  Created by elpaps on 10/13/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import Foundation
import SwiftUI

struct CircleImage: View {
    var image: Image
    var body: some View {
        image
            .clipShape(Circle())
            .overlay(Circle().stroke(Color.white, lineWidth: 4))
            .shadow(radius: 10)
    }
}
